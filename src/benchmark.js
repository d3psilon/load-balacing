const axios = require("axios");

const communes = [];
const nbClient = 100;

for (let index = 20000; index < 20000 + nbClient; index++) {
  communes.push(`com${index}`);
}

console.log(communes.length);

const instance = axios.create({
  baseURL: "http://localhost:3000",
  timeout: 5000,
});

function getBench(customer) {
  instance
    .get("/bench", {
      headers: { customer },
    })
    .then(function (response) {
      // handle success
      // console.log(response.data);
    })
    .catch(function (error) {
      // handle error
      console.log(`${customer} - ${error.message}`);
    })
    .then(function () {
      // always executed
    });
}

const test = async (interval) => {
  setInterval(async () => {
    communes.forEach(async (c) => {
      getBench(c);
      const random = getRandomInt(1000);
      await sleep(random);
    });
  }, interval);
};

test(9000);
test(30000);
test(17000);

async function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

// SHOW max_connections;
// ALTER SYSTEM SET max_connections TO '500'; must restart
