const { Sequelize } = require("sequelize");

const express = require("express");
const process = require("process");
const app = express();
const port = 3000;

const database = "postgres";
const username = "postgres";
const password = "postgres";

const instances = {};

app.get("/", (req, res) => {
  res.send(
    `${req.headers["customer"] || "anonyme"} sur ${process.env["servername"]}`
  );
});

app.get("/bench", (req, res) => {
  const customer = req.headers["customer"] || "anonyme";
  if (isInstance(customer)) {
    const instance = getInstance(customer);
    instance.models.user
      .create({
        firstName: "",
        lastName: "",
        customer,
      })
      .then((result) => {
        res.send(result);
      })
      .catch((err) => {
        res.status(500).send("KO");
      });
  } else {
    const instance = addInstance(customer);
    authenticate(instance)
      .then((result) => {
        res.send(
          `${req.headers["customer"] || "anonyme"} sur ${
            process.env["servername"]
          }`
        );
      })
      .catch((err) => {
        res.status(500).send("KO");
      });
  }
});

async function getInstance(customer) {
  return instances[customer];
}

async function addInstance(customer) {
  const instance = new Sequelize(database, username, password, {
    dialect: "postgres",
    host: process.env["servername"] ? "172.18.0.1" : "localhost",
    port: 5432,
    pool: {
      max: 3,
      min: 0,
      acquire: 10000,
      idle: 10000,
      evict: 15000,
    },
  });

  const User = instance.define(
    "user",
    {
      firstName: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      lastName: {
        type: Sequelize.STRING,
      },
      customer: {
        type: Sequelize.STRING,
      },
    },
    {
      // options
    }
  );

  instances[customer] = instance;

  await User.sync({ force: true })
    .then(() => {
      console.log("Model User OK");
    })
    .catch((err) => {
      console.log(err.message);
    });

  return instance;
}

function isInstance(customer) {
  if (instances[customer]) {
    return true;
  }
  return false;
}

// function authenticate(instance) {
//   return instance.authenticate();
// }

process.on("SIGINT", () => {
  process.exit(0);
});

process.on("SIGTERM", () => {
  process.exit(0);
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
