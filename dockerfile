FROM node:stretch-slim

ADD . /src/
WORKDIR /src
RUN npm install

EXPOSE 3000

CMD npm run start